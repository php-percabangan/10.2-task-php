<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Konversi Nilai</title>
</head>
<body>
    <div class="container">
        <form method="post">
        <h1>Masukkan Nilai</h1>
            <div class = "form-group">
                <input type="number" name="nilai" id="nilai" required>
            </div>
            <div class="form-group">
                <input type="submit" value="Konversi">
            </div>
        </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $nilai = (int)$_POST['nilai'];

        if ($nilai >= 85 && $nilai <= 100) {
            $grade = 'A';
        } elseif ($nilai >= 75 && $nilai < 85) {
            $grade = 'B';
        } elseif ($nilai >= 65 && $nilai < 75) {
            $grade = 'C';
        } elseif ($nilai >= 50 && $nilai < 65) {
            $grade = 'D';
        } elseif ($nilai >= 0 && $nilai < 50) {
            $grade = 'E';
        } else {
            $grade = 'Nilai tidak valid';
        }
        echo "<div class='result'>Nilai Anda $nilai, Anda mendapat Grade : $grade</div>";
    }
    ?>
    <div class='nav-link'>
        <a href="segitiga.php">Masuk ke Pola Segitiga</a>
    </div>
</body>
</html>
