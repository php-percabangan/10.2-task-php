<!DOCTYPE html>
<html>
<head>
    <title>Pembuatan Pola Bintang Segitiga</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
    <div class="container">
        <form method="post">
            <h1>Pembuatan Pola Bintang Segitiga</h1>
            <div class="form-group">
                <input type="number" name="tinggi" id="tinggi" required>
            </div>
            <div class="form-group">
                <input type="submit" value="Buat Pola">
            </div>
        </form>

        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $tinggi = (int)$_POST['tinggi'];

            echo "<div class='result'><pre>";
            for ($i = 1; $i <= $tinggi; $i++) {
                for ($j = $tinggi; $j > $i; $j--) {
                    echo " ";
                }
                for ($k = 0; $k < ($i * 2) - 1; $k++) {
                    echo "*";
                }
                echo "\n";
            }
            echo "</pre></div>";
        }
        ?>
        <div class="nav-link">
            <a href="index.php">Kembali ke Konversi Nilai</a>
        </div>
    </div>
</body>
</html>
